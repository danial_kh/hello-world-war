export const typography = {
	fontFamily: "IRANSans",
	h1: {
		fontWeight: 200,
		fontSize: 48,
		lineHeight: "75px",
	},
	h2: {
		fontWeight: 300,
		fontSize: 34,
		lineHeight: "53px",
	},
	h3: {
		fontWeight: 400,
		fontSize: 24,
		lineHeight: "38px",
	},
	h4: {
		fontWeight: 500,
		fontSize: 20,
		lineHeight: "31px",
	},
	h5: {
		fontWeight: 400,
		fontSize: 24,
		lineHeight: "37.6px",
	},
	body1: {
		fontSize: 16,
		fontWeight: 400,
		lineHeight: "25px",
	},
	body2: {
		fontSize: 14,
		fontWeight: 400,
		lineHeight: "22px",
	},
	subtitle1: {
		fontWeight: "normal",
		fontSize: 16,
		lineHeight: "25px",
	},
	subtitle2: {
		fontWeight: 500,
		fontSize: 14,
		lineHeight: "22px",
	},
	caption: {
		fontSize: 12,
		fontWeight: 400,
		lineHeight: "19px",
	},
};
