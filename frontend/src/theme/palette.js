export const palette = {
	primary: {
		main: "#51AFBC",
		dark: "#19808F",
	},
	secondary: {
		main: "#FF756B",
	},
	error: {
		main: "#FF756B",
	},
	warning: {
		main: "#FF756B",
	},
	success: {
		main: "#81E793",
	},
	text: {
		primary: "#4B4B4B",
		secondary: "#707070",
		success: "#4B4B4B",
	},
};
