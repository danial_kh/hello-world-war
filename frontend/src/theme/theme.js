import { createTheme } from "@material-ui/core/styles";
import { typography } from "./typography";
import { palette } from "./palette";

export const theme = createTheme({
	direction: "rtl",
	transitions: {
		duration: {
			shortest: 150,
			shorter: 200,
			short: 250,
			// most basic recommended timing
			standard: 300,
			// this is to be used in complex animations
			complex: 375,
			// recommended when something is entering screen
			enteringScreen: 225,
			// recommended when something is leaving screen
			leavingScreen: 195,
		},
	},
	breakpoints: {
		values: {
			xs: 0,
			sm: 300,
			md: 600,
			lg: 900,
			xl: 1920,
		},
	},
	overrides: {
		MuiTypography: {
			root: {
				whiteSpace: "pre-line",
			},
		},
		MuiAppBar: {
			colorPrimary: {
				backgroundColor: "#fff",
				color: "#000",
			},
		},
		MuiToolbar: {
			regular: {
				paddingLeft: 0,
				paddingRight: 0,
			},
		},
		MuiButton: {
			root: {
				padding: "12px 16px",
			},
			contained: {
				boxShadow: "none",
				fontWeight: 400,
				fontSize: 15,
				lineHeight: "24px",
			},
			containedPrimary: {
				color: "#fff",
			},
		},
		MuiSelect: {
			outlined: {
				paddingTop: 10.5,
				paddingBottom: 10.5,
			},
		},
		// MuiOutlinedInput: {
		//   notchedOutline: {
		//     borderColor: '#DADADA',
		//   },
		// },
		// MuiFormLabel: {
		//   root: {
		//     color: '#707070',
		//     fontSize: 14,
		//     fontWeight: 'bold',
		//     lineHeight: '16px',
		//   },
		// },
		// MuiChip: {
		//   colorSecondary: {
		//     color: '#fff',
		//   },
		// },
	},
	typography,
	palette,
});
