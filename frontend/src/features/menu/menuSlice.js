import { createSlice } from "@reduxjs/toolkit";

const initialState = {
	value: {
		expand: "expand",
	},
};

export const menuSlice = createSlice({
	name: "menu",
	initialState,
	reducers: {
		expandMenu: (state, action) => {
			state.value.expand = action.payload;
		},
		shrinkMenu: (state, action) => {
			state.value.expand = "shrink";
		},
	},
});

export const { expandMenu, shrinkMenu } = menuSlice.actions;

export const selectMenu = (state) => state.menu.value;

export default menuSlice.reducer;
