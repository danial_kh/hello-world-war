import { createSlice } from "@reduxjs/toolkit";

const initialState = {
	value: [],
};

export const snackSlice = createSlice({
	name: "snack",
	initialState,
	reducers: {
		addSnack: (state, action) => {
			state.value.push(action.payload);
		},
		removeSnack: (state, action) => {
			state.value.splice(action.payload, 1);
		},
	},
});

export const { addSnack, removeSnack } = snackSlice.actions;

export const selectSnack = (state) => state.snack.value;

export default snackSlice.reducer;
