import React, { useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import Snackbar from "@material-ui/core/Snackbar";
import { useEffect } from "react";
import {
	decrement,
	increment,
	incrementByAmount,
	incrementAsync,
	incrementIfOdd,
	selectCount,
} from "./counterSlice";
import { addSnack, removeSnack, selectSnack } from "./snackSlice";
import styles from "./Counter.module.css";
import { ResetTvRounded } from "@mui/icons-material";
import AlertMaker from "../../components/alertMaker";
import Alert from "@material-ui/lab/Alert";
import { useSnackbar } from "notistack";

export function Counter() {
	const count = useSelector(selectCount);
	const snack = useSelector(selectSnack);
	const dispatch = useDispatch();
	const [incrementAmount, setIncrementAmount] = useState("2");

	const incrementValue = Number(incrementAmount) || 0;

	return (
		<div>
			<div className={styles.row}>
				<button
					className={styles.button}
					aria-label="Decrement value"
					onClick={() => dispatch(decrement())}
				>
					-
				</button>
				<span className={styles.value}>{count}</span>
				<button
					className={styles.button}
					aria-label="Increment value"
					onClick={() => dispatch(increment())}
				>
					+
				</button>
			</div>
			<div className={styles.row}>
				<input
					className={styles.textbox}
					aria-label="Set increment amount"
					value={incrementAmount}
					onChange={(e) => setIncrementAmount(e.target.value)}
				/>
				<button
					className={styles.button}
					onClick={() => dispatch(incrementByAmount(incrementValue))}
				>
					Add Amount
				</button>
				<button
					className={styles.asyncButton}
					onClick={() => dispatch(incrementAsync(incrementValue))}
				>
					Add Async
				</button>
				<button
					className={styles.button}
					onClick={() => dispatch(incrementIfOdd(incrementValue))}
				>
					Add If Odd
				</button>
			</div>
			<AlertMaker />
			{snack.map((name, index) => {
				return (
					<Alert
						severity="warning"
						index={index}
						onClick={() => dispatch(removeSnack(index))}
					>
						{name}
					</Alert>
				);
			})}
		</div>
	);
}
