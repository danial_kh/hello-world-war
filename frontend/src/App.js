import React, { Fragment } from "react";
import logo from "./logo.svg";
import { Counter } from "./features/counter/Counter";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import "./App.css";
import { useSelector, useDispatch } from "react-redux";
import { SnackbarProvider, useSnackbar } from "notistack";
import RTL from "./components/RTL";
import {
	addSnack,
	removeSnack,
	selectSnack,
} from "./features/counter/snackSlice";
import HomePage from "./pages/Home";
import DashboardPage from "./pages/Dashboard";
import LoginPage from "./pages/Signin";
function App() {
	return (
		<Router>
			<RTL>
				<SnackbarProvider maxSnack={5}>
					<Routes>
						<Route path="/" element={<LoginPage />} />
						<Route path="/dashboard" element={<DashboardPage />} />
						<Route path="/login" element={<LoginPage />} />
					</Routes>
				</SnackbarProvider>
			</RTL>
		</Router>
	);
}

export default App;
