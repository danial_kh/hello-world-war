import React from "react";
import { makeStyles, useMediaQuery, useTheme } from "@material-ui/core";
import LoginDesktop from "../views/Signin/Desktop";
import LoginMobile from "../views/Signin/Mobile";
const useStyles = makeStyles({
	root: {
		paddingTop: 0,
		width: "100%",
		height: "100%",
	},
});

export default function LoginPage() {
	const classes = useStyles();
	const theme = useTheme();
	const mobileView = useMediaQuery(theme.breakpoints.down("sm"));
	function displayMobile() {
		return <LoginMobile />;
	}

	function displayDesktop() {
		return <LoginDesktop />;
	}

	return (
		<section className={classes.root}>
			{mobileView ? displayMobile() : displayDesktop()}
		</section>
	);
}
