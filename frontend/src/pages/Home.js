import React from "react";
import { makeStyles, useMediaQuery, useTheme } from "@material-ui/core";
import HomeDesktop from "../views/Home/Desktop";
import HomeMobile from "../views/Home/mobile";
const useStyles = makeStyles({
	root: {
		paddingTop: 86,
		backgroundRepeat: "no-repeat",
		backgroundSize: "cover",
		backgroundPositionX: "right",
	},
	button: {
		fontWeight: 500,
		display: "block",
		width: "100%",
		margin: "24px auto 0",
		textAlign: "center",
		color: "white",
		backgroundColor: "#5AD76F",
		padding: "10px 16px",

		"&:hover": {
			backgroundColor: "#3BC553",
		},
	},
	container: {
		display: "flex",
		flexDirection: "column",
		alignItems: "center",
		marginTop: "10%",
		height: "100%",
	},
	gridContainer: {
		maxWidth: "1000px",

		marginBottom: "56px",
		paddingTop: "36px",
		paddingRight: "16px",
		paddingLeft: "16px",
		paddingBottom: "16px",
	},
	sections: {
		height: "100%",
	},
	titleContainer: {
		color: "#143156",

		"& h1": {
			fontWeight: 900,
			fontSize: 60,
			marginBottom: 15,
			marginTop: 15,
		},
		"& h4": {
			fontWeight: 300,
			fontSize: 40,
		},
		"& h6": {
			fontWeight: 300,
			fontSize: 25,
		},
	},
});

export default function HomePage() {
	const classes = useStyles();
	const theme = useTheme();
	const mobileView = useMediaQuery(theme.breakpoints.down("sm"));
	function displayMobile() {
		return <HomeMobile />;
	}

	function displayDesktop() {
		return <HomeDesktop />;
	}

	return (
		<section className={classes.root}>
			{mobileView ? displayMobile() : displayDesktop()}
		</section>
	);
}
