import React from "react";
import { useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import { useSnackbar } from "notistack";
import {
	addSnack,
	removeSnack,
	selectSnack,
} from "../features/counter/snackSlice";
export default function AlertMaker() {
	const { enqueueSnackbar, closeSnackbar } = useSnackbar();
	const dispatch = useDispatch();
	const [alertMessage, setAlertMessage] = useState("");

	function handleClick(Message) {
		dispatch(addSnack(Message));
		enqueueSnackbar(Message);
	}

	return (
		<div>
			<TextField
				label="Outlined"
				variant="outlined"
				onChange={(e) => setAlertMessage(e.target.value)}
			/>
			<Button
				variant="contained"
				color="secondary"
				onClick={() => handleClick(alertMessage)}
			>
				add Alert!
			</Button>
		</div>
	);
}
