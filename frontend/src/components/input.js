import React from "react";
import TextField from "@material-ui/core/TextField";
import { makeStyles } from "@material-ui/core/styles";
import RTL from "./RTL";
import { createTheme, ThemeProvider } from "@mui/material/styles";

const useStyles = makeStyles((theme) => ({
	root: {
		width: "calc(100% - 72px)",
		marginBottom: 32,
		direction: "ltr",
		textAlign: "left",
		flip: false,
		"& .MuiInputLabel-outlined": {
			fontSize: "12pt",

			color: "#707070",
		},
		"& label.Mui-focused": {
			color: "#707070",
		},
		"& .MuiInput-underline:after": {
			borderBottomColor: "#DADADA",
		},
		"& .MuiOutlinedInput-root": {
			fontSize: "12pt",
			color: "#707070",

			"&:hover": {
				fontSize: "12pt",
				color: "#3C3C3C",
			},
			"&:focused": {
				fontSize: "4pt",
			},
			"& fieldset": {
				borderColor: "#DADADA",
			},
			"&:hover fieldset": {
				borderColor: "#3C3C3C",
			},
			"&.Mui-focused fieldset": {
				borderColor: "#707070",
			},
		},
	},
	input: {
		textAlign: "center",
	},
}));
export default function CustomInput() {
	const classes = useStyles();
	return (
		<TextField
			id="outlined-basic"
			label="نام کاربری"
			variant="outlined"
			size="small"
			className={classes.root}
			inputProps={{ className: classes.input }}
		/>
	);
}
