import React from "react";
import { Grid } from "@material-ui/core";
import { makeStyles, useMediaQuery, useTheme } from "@material-ui/core";
import contents from "./contents";
const useStyles = makeStyles({
	root: {
		textAlign: "center",
		display: "flex",
		justifyContent: "center",
		alignItems: "center",
	},
});

export default function DesktopFooter() {
	const classes = useStyles();
	return (
		<div className={classes.root}>
			<p>{contents.footer}</p>
		</div>
	);
}
