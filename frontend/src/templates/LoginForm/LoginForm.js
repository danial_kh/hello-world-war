import React from "react";
import { useState } from "react";
import CustomInput from "../../components/input";
import { useSnackbar } from "notistack";
import {
	makeStyles,
	TextField,
	Grid,
	Typography,
	Button,
	CircularProgress,
	Switch,
} from "@material-ui/core";
import contents from "./contents";

const useStyles = makeStyles((theme) => ({
	form: {
		marginTop: "10%",
		minWidth: "250px",
		color: "#707070",
		backgroundColor: "#fafafa",
		height: "100%",
	},
	textField: {
		// width: "calc(100% - 16px)",
		width: "100%",
		marginBottom: 24,
	},
	input: {
		textAlign: "center",
	},
	submitBtn: {
		// width: "calc(100% - 16px)",
		width: "100%",
	},
	caption: {
		fontSize: "10pt",
		fontWeight: 100,
	},
	title: {
		fontSize: "24pt",
		fontWeight: 200,
		color: "#31919F",
	},
}));
export default function LoginForm() {
	const classes = useStyles();
	const [loading, setLoading] = useState(true);
	const [remember, setRemember] = useState(false);
	const { enqueueSnackbar } = useSnackbar();
	const handleClick = () => {
		enqueueSnackbar(
			"پنل کاربری در دست طراحی است و به زودی امکان دسترسی فراهم می گردد.",
			"primary"
		);
	};
	function handleRemember() {
		setRemember(!remember);
	}
	return (
		<Grid container direction="row" component="div">
			<Grid item xs={1} md={2} lg={4}></Grid>
			<Grid item xs={10} md={8} lg={4} className={classes.form}>
				<h2 className={classes.title}>{contents.form.enter}</h2>
				<h3 className={classes.caption}>{contents.form.enterdata}</h3>
				<TextField
					className={classes.textField}
					inputProps={{ className: classes.input }}
					label="نام کاربری"
					variant="outlined"
					size="small"
					type="text"
				/>
				<TextField
					className={classes.textField}
					inputProps={{ className: classes.input }}
					label="رمز عبور"
					variant="outlined"
					size="small"
					type="password"
				/>
				<Switch color="primary" checked={remember} onClick={handleRemember} />
				<Typography
					component="span"
					color={remember ? "primary" : "initial"}
					className={classes.caption}
				>
					{contents.form.rememberme}
				</Typography>
				<Button
					className={classes.submitBtn}
					variant="contained"
					color="primary"
					type="submit"
					onClick={handleClick}
					disabled={loading}
				>
					{loading ? (
						<CircularProgress size={22} />
					) : (
						<Typography variant="subtitle2"> تایید و ارسال اطلاعات</Typography>
					)}
				</Button>
			</Grid>
			<Grid item xs={1} md={2} lg={4}></Grid>
		</Grid>
	);
}
