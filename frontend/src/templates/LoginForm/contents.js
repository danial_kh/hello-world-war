const contents = {
	commingSoon: "به زودی...",
	form: {
		enter: "ورود",
		enterdata: "لطفا نام کاربری و رمز عبور خود را وارد نمایید.",
		rememberme: "رمز عبور ذخیره شود"
	},
	footer: "تمامی حقوق متعلق به شرکت میلاد دارو نور است.",
};

export default contents;
