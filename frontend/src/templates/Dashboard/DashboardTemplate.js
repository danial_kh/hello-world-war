import React from "react";
import { useState } from "react";
import CustomInput from "../../components/input";
import { useSnackbar } from "notistack";
import {
	makeStyles,
	TextField,
	Grid,
	Typography,
	Button,
	CircularProgress,
	Switch,
	Card,
	CardContent,
} from "@material-ui/core";
import contents from "./contents";
import { ResponsiveLine } from "@nivo/line";
import { ResponsiveTreeMap } from "@nivo/treemap";
const useStyles = makeStyles((theme) => ({
	form: {
		marginTop: "10%",
		minWidth: "250px",
		color: "#707070",
		backgroundColor: "#fafafa",
		height: "100%",
	},
	textField: {
		// width: "calc(100% - 16px)",
		width: "100%",
		marginBottom: 24,
	},
	input: {
		textAlign: "center",
	},
	submitBtn: {
		// width: "calc(100% - 16px)",
		width: "100%",
	},
	caption: {
		fontSize: "10pt",
		fontWeight: 100,
	},
	title: {
		fontSize: "24pt",
		fontWeight: 200,
		color: "#31919F",
	},
	chart: {
		minHeight: "300px",
		height: "600px",
		direction: "ltr",
	},
	card: {
		margin: "36px 0 0 0",
	},
}));
export default function DashboardTemplate() {
	const classes = useStyles();
	const [loading, setLoading] = useState(false);
	const [remember, setRemember] = useState(false);
	const { enqueueSnackbar } = useSnackbar();
	const handleClick = () => {
		enqueueSnackbar(
			"پنل کاربری در دست طراحی است و به زودی امکان دسترسی فراهم می گردد.",
			"primary"
		);
	};
	function handleRemember() {
		setRemember(!remember);
	}

	const MyResponsiveLine = ({ data /* see data tab */ }) => (
		<ResponsiveLine
			data={data}
			margin={{ top: 50, right: 110, bottom: 50, left: 60 }}
			xScale={{ type: "point" }}
			yScale={{
				type: "linear",
				min: "auto",
				max: "auto",
				stacked: true,
				reverse: false,
			}}
			yFormat=" >-.2f"
			curve="cardinal"
			axisTop={null}
			axisRight={null}
			axisBottom={{
				orient: "bottom",
				tickSize: 5,
				tickPadding: 5,
				tickRotation: 0,
				legend: "تولید",
				legendOffset: 36,
				legendPosition: "middle",
			}}
			axisLeft={{
				orient: "left",
				tickSize: 0,
				tickPadding: 35,
				tickRotation: 0,
				legend: "تعداد",
				legendOffset: -45,
				legendPosition: "middle",
			}}
			enableGridX={false}
			enableGridY={false}
			lineWidth={4}
			pointSize={17}
			pointColor="white"
			pointBorderWidth={5}
			pointBorderColor={{ from: "serieColor" }}
			pointLabelYOffset={-12}
			useMesh={true}
			legends={[
				{
					anchor: "bottom-right",
					direction: "column",
					justify: true,
					translateX: 80,
					translateY: 30,
					itemsSpacing: 0,
					itemDirection: "left-to-right",
					itemWidth: 30,
					itemHeight: 50,
					itemOpacity: 0.75,
					symbolSize: 15,
					symbolShape: "circle",
					symbolBorderColor: "rgba(0, 0, 0, .5)",
					effects: [
						{
							on: "hover",
							style: {
								itemBackground: "rgba(0, 0, 0, .03)",
								itemOpacity: 1,
							},
						},
					],
				},
			]}
		/>
	);
	const MyResponsiveTreeMap = ({ data /* see data tab */ }) => (
		<ResponsiveTreeMap
			data={data}
			identity="name"
			value="loc"
			valueFormat=".02s"
			innerPadding={2}
			outerPadding={0}
			margin={{ top: 10, right: 10, bottom: 10, left: 10 }}
			label="id"
			labelSkipSize={12}
			labelTextColor={{ from: "color", modifiers: [["darker", 1.2]] }}
			parentLabelTextColor={{ from: "color", modifiers: [["darker", 2]] }}
			parentLabelPadding={30}
			nodeOpacity={0.15}
			enableParentLabel={false}
			borderColor={{ from: "color", modifiers: [["darker", 0.1]] }}
		/>
	);
	return (
		<Grid
			container
			direction="row"
			component="div"
			justifyContent="space-around"
		>
			<Grid component="div" md={11}>
				<Card sx={{ minWidth: 275 }}>
					<CardContent className={classes.chart} component="div">
						<MyResponsiveLine data={contents.chartData} />
					</CardContent>
				</Card>
			</Grid>
			<Grid component="div" md={11} className={classes.card}>
				<Card sx={{ minWidth: 275 }}>
					<CardContent className={classes.chart} component="div">
						<MyResponsiveTreeMap data={contents.treeMapData} />
					</CardContent>
				</Card>
			</Grid>
			<p>this is dasahboard template</p>
		</Grid>
	);
}
