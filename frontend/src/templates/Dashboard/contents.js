const contents = {
	commingSoon: "به زودی...",
	form: {
		enter: "ورود",
		enterdata: "لطفا نام کاربری و رمز عبور خود را وارد نمایید.",
		rememberme: "رمز عبور ذخیره شود",
	},
	footer: "تمامی حقوق متعلق به شرکت میلاد دارو نور است.",
	treeMapData: {
		name: "nivo",
		color: "hsl(102, 70%, 50%)",
		children: [
			{
				name: "viz",
				color: "hsl(215, 70%, 50%)",
				children: [
					{
						name: "stack",
						color: "hsl(3, 70%, 50%)",
						children: [
							{
								name: "cchart",
								color: "hsl(287, 70%, 50%)",
								loc: 182872,
							},
							{
								name: "xAxis",
								color: "hsl(103, 70%, 50%)",
								loc: 54851,
							},
							{
								name: "yAxis",
								color: "hsl(296, 70%, 50%)",
								loc: 59841,
							},
							{
								name: "layers",
								color: "hsl(165, 70%, 50%)",
								loc: 42791,
							},
						],
					},
					{
						name: "ppie",
						color: "hsl(284, 70%, 50%)",
						children: [
							{
								name: "chart",
								color: "hsl(359, 70%, 50%)",
								children: [
									{
										name: "pie",
										color: "hsl(89, 70%, 50%)",
										children: [
											{
												name: "outline",
												color: "hsl(23, 70%, 50%)",
												loc: 81483,
											},
											{
												name: "slices",
												color: "hsl(196, 70%, 50%)",
												loc: 199558,
											},
											{
												name: "bbox",
												color: "hsl(212, 70%, 50%)",
												loc: 63207,
											},
										],
									},
									{
										name: "donut",
										color: "hsl(105, 70%, 50%)",
										loc: 33307,
									},
									{
										name: "gauge",
										color: "hsl(181, 70%, 50%)",
										loc: 7715,
									},
								],
							},
							{
								name: "legends",
								color: "hsl(55, 70%, 50%)",
								loc: 64344,
							},
						],
					},
				],
			},
			{
				name: "colors",
				color: "hsl(359, 70%, 50%)",
				children: [
					{
						name: "rgb",
						color: "hsl(109, 70%, 50%)",
						loc: 8215,
					},
					{
						name: "hsl",
						color: "hsl(250, 70%, 50%)",
						loc: 100160,
					},
				],
			},
			{
				name: "utils",
				color: "hsl(335, 70%, 50%)",
				children: [
					{
						name: "randomize",
						color: "hsl(72, 70%, 50%)",
						loc: 176164,
					},
					{
						name: "resetClock",
						color: "hsl(109, 70%, 50%)",
						loc: 150417,
					},
					{
						name: "noop",
						color: "hsl(36, 70%, 50%)",
						loc: 135785,
					},
					{
						name: "tick",
						color: "hsl(77, 70%, 50%)",
						loc: 87017,
					},
					{
						name: "forceGC",
						color: "hsl(295, 70%, 50%)",
						loc: 172297,
					},
					{
						name: "stackTrace",
						color: "hsl(15, 70%, 50%)",
						loc: 176265,
					},
					{
						name: "dbg",
						color: "hsl(80, 70%, 50%)",
						loc: 139145,
					},
				],
			},
			{
				name: "generators",
				color: "hsl(92, 70%, 50%)",
				children: [
					{
						name: "address",
						color: "hsl(180, 70%, 50%)",
						loc: 102820,
					},
					{
						name: "city",
						color: "hsl(23, 70%, 50%)",
						loc: 28821,
					},
					{
						name: "animal",
						color: "hsl(100, 70%, 50%)",
						loc: 74153,
					},
					{
						name: "movie",
						color: "hsl(291, 70%, 50%)",
						loc: 129780,
					},
					{
						name: "user",
						color: "hsl(356, 70%, 50%)",
						loc: 14699,
					},
				],
			},
			{
				name: "set",
				color: "hsl(91, 70%, 50%)",
				children: [
					{
						name: "clone",
						color: "hsl(26, 70%, 50%)",
						loc: 68769,
					},
					{
						name: "intersect",
						color: "hsl(340, 70%, 50%)",
						loc: 79338,
					},
					{
						name: "merge",
						color: "hsl(117, 70%, 50%)",
						loc: 196868,
					},
					{
						name: "reverse",
						color: "hsl(23, 70%, 50%)",
						loc: 59290,
					},
					{
						name: "toArray",
						color: "hsl(188, 70%, 50%)",
						loc: 47713,
					},
					{
						name: "toObject",
						color: "hsl(225, 70%, 50%)",
						loc: 184636,
					},
					{
						name: "fromCSV",
						color: "hsl(341, 70%, 50%)",
						loc: 107093,
					},
					{
						name: "slice",
						color: "hsl(149, 70%, 50%)",
						loc: 108826,
					},
					{
						name: "append",
						color: "hsl(33, 70%, 50%)",
						loc: 42372,
					},
					{
						name: "prepend",
						color: "hsl(216, 70%, 50%)",
						loc: 126077,
					},
					{
						name: "shuffle",
						color: "hsl(25, 70%, 50%)",
						loc: 134499,
					},
					{
						name: "pick",
						color: "hsl(89, 70%, 50%)",
						loc: 106432,
					},
					{
						name: "plouc",
						color: "hsl(358, 70%, 50%)",
						loc: 134257,
					},
				],
			},
			{
				name: "text",
				color: "hsl(227, 70%, 50%)",
				children: [
					{
						name: "trim",
						color: "hsl(113, 70%, 50%)",
						loc: 12047,
					},
					{
						name: "slugify",
						color: "hsl(176, 70%, 50%)",
						loc: 135304,
					},
					{
						name: "snakeCase",
						color: "hsl(232, 70%, 50%)",
						loc: 176328,
					},
					{
						name: "camelCase",
						color: "hsl(284, 70%, 50%)",
						loc: 142031,
					},
					{
						name: "repeat",
						color: "hsl(18, 70%, 50%)",
						loc: 132791,
					},
					{
						name: "padLeft",
						color: "hsl(299, 70%, 50%)",
						loc: 102523,
					},
					{
						name: "padRight",
						color: "hsl(174, 70%, 50%)",
						loc: 64710,
					},
					{
						name: "sanitize",
						color: "hsl(250, 70%, 50%)",
						loc: 174140,
					},
					{
						name: "ploucify",
						color: "hsl(103, 70%, 50%)",
						loc: 20243,
					},
				],
			},
			{
				name: "misc",
				color: "hsl(278, 70%, 50%)",
				children: [
					{
						name: "greetings",
						color: "hsl(9, 70%, 50%)",
						children: [
							{
								name: "hey",
								color: "hsl(4, 70%, 50%)",
								loc: 194538,
							},
							{
								name: "HOWDY",
								color: "hsl(52, 70%, 50%)",
								loc: 36331,
							},
							{
								name: "aloha",
								color: "hsl(46, 70%, 50%)",
								loc: 62746,
							},
							{
								name: "AHOY",
								color: "hsl(335, 70%, 50%)",
								loc: 192534,
							},
						],
					},
					{
						name: "other",
						color: "hsl(26, 70%, 50%)",
						loc: 181394,
					},
					{
						name: "path",
						color: "hsl(274, 70%, 50%)",
						children: [
							{
								name: "pathA",
								color: "hsl(79, 70%, 50%)",
								loc: 119381,
							},
							{
								name: "pathB",
								color: "hsl(216, 70%, 50%)",
								children: [
									{
										name: "pathB1",
										color: "hsl(190, 70%, 50%)",
										loc: 15494,
									},
									{
										name: "pathB2",
										color: "hsl(292, 70%, 50%)",
										loc: 94729,
									},
									{
										name: "pathB3",
										color: "hsl(62, 70%, 50%)",
										loc: 14475,
									},
									{
										name: "pathB4",
										color: "hsl(351, 70%, 50%)",
										loc: 67194,
									},
								],
							},
							{
								name: "pathC",
								color: "hsl(93, 70%, 50%)",
								children: [
									{
										name: "pathC1",
										color: "hsl(255, 70%, 50%)",
										loc: 71890,
									},
									{
										name: "pathC2",
										color: "hsl(143, 70%, 50%)",
										loc: 712,
									},
									{
										name: "pathC3",
										color: "hsl(5, 70%, 50%)",
										loc: 113123,
									},
									{
										name: "pathC4",
										color: "hsl(74, 70%, 50%)",
										loc: 83522,
									},
									{
										name: "pathC5",
										color: "hsl(139, 70%, 50%)",
										loc: 13554,
									},
									{
										name: "pathC6",
										color: "hsl(110, 70%, 50%)",
										loc: 111607,
									},
									{
										name: "pathC7",
										color: "hsl(82, 70%, 50%)",
										loc: 150534,
									},
									{
										name: "pathC8",
										color: "hsl(200, 70%, 50%)",
										loc: 105048,
									},
									{
										name: "pathC9",
										color: "hsl(48, 70%, 50%)",
										loc: 191028,
									},
								],
							},
						],
					},
				],
			},
		],
	},
	chartData: [
		{
			id: "مهر",
			color: "hsl(343, 70%, 50%)",
			data: [
				{
					x: "step1",
					y: 300,
				},
				{
					x: "step2",
					y: 155,
				},
				{
					x: "Quality Control",
					y: 191,
				},
				{
					x: "Labeling",
					y: 288,
				},
				{
					x: "Packaging",
					y: 133,
				},
				{
					x: "Storing",
					y: 260,
				},
			],
		},
		{
			id: "آبان",
			color: "hsl(165, 70%, 50%)",
			data: [
				{
					x: "step1",
					y: 8,
				},
				{
					x: "step2",
					y: 203,
				},
				{
					x: "Quality Control",
					y: 163,
				},
				{
					x: "Labeling",
					y: 48,
				},
				{
					x: "Packaging",
					y: 122,
				},
				{
					x: "Storing",
					y: 27,
				},
			],
		},
		{
			id: "آذر",
			color: "hsl(210, 70%, 50%)",
			data: [
				{
					x: "step1",
					y: 217,
				},
				{
					x: "step2",
					y: 47,
				},
				{
					x: "Quality Control",
					y: 220,
				},
				{
					x: "Labeling",
					y: 151,
				},
				{
					x: "Packaging",
					y: 240,
				},
				{
					x: "Storing",
					y: 201,
				},
			],
		},
		{
			id: "دی",
			color: "hsl(226, 70%, 50%)",
			data: [
				{
					x: "step1",
					y: 236,
				},
				{
					x: "step2",
					y: 192,
				},
				{
					x: "Quality Control",
					y: 13,
				},
				{
					x: "Labeling",
					y: 24,
				},
				{
					x: "Packaging",
					y: 251,
				},
				{
					x: "Storing",
					y: 286,
				},
			],
		},
		{
			id: "بهمن",
			color: "hsl(208, 70%, 50%)",
			data: [
				{
					x: "step1",
					y: 29,
				},
				{
					x: "step2",
					y: 139,
				},
				{
					x: "Quality Control",
					y: 279,
				},
				{
					x: "Labeling",
					y: 237,
				},
				{
					x: "Packaging",
					y: 172,
				},
				{
					x: "Storing",
					y: 52,
				},
			],
		},
	],
};

export default contents;
