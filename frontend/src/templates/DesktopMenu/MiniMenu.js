import React from "react";
import { Grid, Link } from "@material-ui/core";
import { makeStyles, useMediaQuery, useTheme } from "@material-ui/core";
import contents from "./contents";
import Tooltip from "@material-ui/core/Tooltip";
import logo from "./logo.png";
import AccountCircleIcon from "@material-ui/icons/AccountCircle";
import Avatar from "@material-ui/core/Avatar";
import ProfilePic from "./profilepic.jpg";
import ExitToAppIcon from "@material-ui/icons/ExitToApp";
import Divider from "@material-ui/core/Divider";
import Button from "@material-ui/core/Button";
import IconButton from "@material-ui/core/IconButton";
import ArrowLeftIcon from "@material-ui/icons/ArrowLeft";
import { useSelector, useDispatch } from "react-redux";
import {
	selectMenu,
	shrinkMenu,
	expandMenu,
} from "../../features/menu/menuSlice";

const useStyles = makeStyles({
	root: {
		textAlign: "center",
		backgroundColor: "#202020",
		color: "#E4E4E4",
		padding: "0",
		height: "100%",
		position: "fixed",
		width: "78px",
		left: "0px",
	},
	items: {
		textAlign: "center",
		backgroundColor: "#202020",
		color: "#E4E4E4",
		padding: "78px 0 16px 0",
		height: "calc(100% - (64px + 78px + 188px))",
		display: "flex",
		flexDirection: "column",
		alignItems: "center",
		zIndex: "0",
	},
	menuItem: {
		height: "36px",
		// border: "1px solid #FAFAFA",
		width: "36px",
		marginTop: "4px",
		marginBottom: "4px",
		padding: "0 0 0 0",
		color: "#979797",
		cursor: "pointer",
		display: "flex",
		flexShrink: "0",

		alignItems: "center",
		justifyContent: "center",
		borderRadius: "8px",
		"&:hover": {
			backgroundColor: "#3C3C3C",
			color: "#FFFFFF",
		},
	},
	itemName: {
		margin: "0 0 0 8px",
	},
	logoContainer: {},
	logotype: {
		margin: "16px 0 0 0",
		width: "24px",
	},
	profile: {
		height: "120px",
		maxWidth: "56px",
		backgroundColor: "#3C3C3C",
		borderRadius: "8px",
		display: "flex",
		flexDirection: "column",
		alignItems: "center",
		margin: "0 0 16px 0",
		zIndex: "0",
	},
	profilePic: {
		width: "48px",
		height: "48px",
		margin: "8px 0  0",
	},
	userName: {
		fontSize: "20pt",
		fontWeight: "10",
		margin: "8px 0 0 0",
	},
	userRole: {
		fontSize: "10pt",
		fontWeight: "200",
		margin: "0 0 0 0",
	},
	exitButton: {
		height: "36px",
		// border: "1px solid #FAFAFA",

		marginTop: "8px",
		marginBottom: "4px",
		padding: "4px 8px 4px 8px",
		color: "#979797",
		cursor: "pointer",
		display: "flex",

		alignItems: "center",
		borderRadius: "8px",
		"&:hover": {
			backgroundColor: "#202020",
			color: "#FFFFFF",
		},
	},
	arrow: {
		height: "36px",
		width: "36px",
		position: "fixed",
		backgroundColor: "#3C3C3C",
		color: "#FFFFFF",
		top: "60px",
		left: "60px",
		padding: "4 4 4 4",
		zIndex: "1",
		"&:hover": {
			backgroundColor: "#3C3C3C",
		},
	},
});

export default function MiniMenu() {
	const classes = useStyles();

	const dispatch = useDispatch();
	return (
		<Grid
			container
			className={classes.root}
			justifyContent="center"
			alignItems="flex-start"
		>
			<Grid md={12} className={classes.logoContainer}>
				<img src={logo} className={classes.logotype} />
			</Grid>
			<Grid>
				<Tooltip
					title={contents.expandible.expand}
					aria-label={contents.expandible.expand}
				>
					<IconButton
						aria-label="expand"
						small
						className={classes.arrow}
						onClick={() => dispatch(expandMenu("expand"))}
					>
						<ArrowLeftIcon />
					</IconButton>
				</Tooltip>
			</Grid>
			<Grid className={classes.items} md={12}>
				{contents.pages.map((page, index) => (
					<div className={classes.menuItem}>
						<Tooltip title={page.fa} aria-label={page.fa}>
							{page.icon}
						</Tooltip>
					</div>
				))}
			</Grid>

			<Grid md={12} className={classes.profile}>
				<Grid item>
					<Avatar
						alt={contents.profile.name}
						src={ProfilePic}
						className={classes.profilePic}
					>
						{contents.profile.name}
					</Avatar>
				</Grid>

				<Grid item>
					<div className={classes.exitButton}>
						<Tooltip
							title={contents.profile.exit}
							aria-label={contents.profile.exit}
						>
							<ExitToAppIcon />
						</Tooltip>
					</div>
				</Grid>
			</Grid>
		</Grid>
	);
}
