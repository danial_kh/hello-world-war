import React from "react";
import { Grid, Link } from "@material-ui/core";
import { makeStyles, useMediaQuery, useTheme } from "@material-ui/core";
import contents from "./contents";
import Tooltip from "@material-ui/core/Tooltip";
import logotype from "../../views/Signin/logotype.png";
import AccountCircleIcon from "@material-ui/icons/AccountCircle";
import Avatar from "@material-ui/core/Avatar";
import ProfilePic from "./profilepic.jpg";
import ExitToAppIcon from "@material-ui/icons/ExitToApp";
import IconButton from "@material-ui/core/IconButton";
import ArrowRightIcon from "@material-ui/icons/ArrowRight";
import { useSelector, useDispatch } from "react-redux";
import {
	selectMenu,
	shrinkMenu,
	expandMenu,
} from "../../features/menu/menuSlice";

const useStyles = makeStyles({
	root: {
		textAlign: "center",
		backgroundColor: "#202020",
		color: "#E4E4E4",
		padding: "0",
		height: "100%",
		position: "fixed",
		width: "240px",
		left: "0px",
	},
	items: {
		textAlign: "center",
		backgroundColor: "#202020",
		color: "#E4E4E4",
		padding: "78px 0 0 0",
		height: "calc(100% - (64px + 78px + 360px))",
		display: "flex",
		flexDirection: "column",
		alignItems: "center",
	},
	menuItem: {
		height: "36px",
		// border: "1px solid #FAFAFA",
		width: "204px",
		marginTop: "4px",
		marginBottom: "4px",
		padding: "0 0 0 8px",
		color: "#979797",
		cursor: "pointer",
		display: "flex",

		alignItems: "center",
		borderRadius: "8px",
		"&:hover": {
			backgroundColor: "#3C3C3C",
			color: "#FFFFFF",
		},
	},
	itemName: {
		margin: "0 0 0 8px",
	},
	logoContainer: {},
	logotype: {
		margin: "16px 0 0 0",
		width: "204px",
	},
	profile: {
		height: "240px",
		maxWidth: "204px",
		backgroundColor: "#3C3C3C",
		borderRadius: "8px",
		display: "flex",
		flexDirection: "column",
		alignItems: "center",
	},
	profilePic: {
		width: "78px",
		height: "78px",
		margin: "16px 0 0 calc(50% - 39px)",
	},
	userName: {
		fontSize: "20pt",
		fontWeight: "10",
		margin: "8px 0 0 0",
	},
	userRole: {
		fontSize: "10pt",
		fontWeight: "200",
		margin: "0 0 0 0",
	},
	arrow: {
		height: "36px",
		width: "36px",
		position: "fixed",
		backgroundColor: "#3C3C3C",
		color: "#FFFFFF",
		top: "60px",
		left: "220px",
		padding: "4 4 4 4",
		zIndex: "1",
		"&:hover": {
			backgroundColor: "#3C3C3C",
		},
	},
	exitButton: {
		height: "36px",
		// border: "1px solid #FAFAFA",

		marginTop: "16px",
		marginBottom: "4px",
		padding: "4px 8px 4px 8px",
		color: "#979797",
		cursor: "pointer",
		display: "flex",

		alignItems: "center",
		borderRadius: "8px",
		"&:hover": {
			backgroundColor: "#202020",
			color: "#FFFFFF",
		},
	},
});

export default function DesktopMenu() {
	const classes = useStyles();
	const dispatch = useDispatch();
	return (
		<Grid
			container
			className={classes.root}
			justifyContent="center"
			alignItems="flex-start"
		>
			<Grid md={12} className={classes.logoContainer}>
				<img src={logotype} className={classes.logotype} />
			</Grid>
			<Grid>
				<Tooltip
					title={contents.expandible.shrink}
					aria-label={contents.expandible.shrink}
				>
					<IconButton
						aria-label="shrink"
						small
						className={classes.arrow}
						onClick={() => dispatch(expandMenu("shrink"))}
					>
						<ArrowRightIcon />
					</IconButton>
				</Tooltip>
			</Grid>
			<Grid className={classes.items} md={12}>
				{contents.pages.map((page, index) => (
					<div className={classes.menuItem}>
						<Tooltip title={page.fa} aria-label={page.fa}>
							{page.icon}
						</Tooltip>
						<p index={index} className={classes.itemName}>
							{page.fa}
						</p>
					</div>
				))}
			</Grid>
			<Grid md={12} className={classes.profile}>
				<Grid item>
					<Avatar
						alt={contents.profile.name}
						src={ProfilePic}
						className={classes.profilePic}
					>
						{contents.profile.name}
					</Avatar>
				</Grid>
				<Grid item>
					<p className={classes.userName}>{contents.profile.name}</p>
					<p className={classes.userRole}>{contents.profile.role}</p>
				</Grid>
				<Grid item>
					<div className={classes.exitButton}>
						<Tooltip
							title={contents.profile.exit}
							aria-label={contents.profile.exit}
						>
							<ExitToAppIcon />
						</Tooltip>
						<p>{contents.profile.exit}</p>
					</div>
				</Grid>
			</Grid>
		</Grid>
	);
}
