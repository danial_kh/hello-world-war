import DashboardIcon from "@material-ui/icons/Dashboard";
import SettingsIcon from "@material-ui/icons/Settings";
import PeopleIcon from "@material-ui/icons/People";
import TimelapseIcon from "@material-ui/icons/Timelapse";
import EqualizerIcon from "@material-ui/icons/Equalizer";
import MessageIcon from "@material-ui/icons/Message";
const contents = {
	commingSoon: "به زودی...",
	pages: [
		{
			name: "dashboard",
			fa: "داشبورد",
			icon: <DashboardIcon />,
		},

		{ name: "users", fa: "کاربران", icon: <PeopleIcon /> },
		{ name: "shift", fa: "شیفت کاری", icon: <TimelapseIcon /> },
		{ name: "charts", fa: "نمودار ها", icon: <EqualizerIcon /> },
		{ name: "messages", fa: "پیام ها", icon: <MessageIcon /> },
		{ name: "settings", fa: "تنظیمات", icon: <SettingsIcon /> },
	],
	profile: {
		name: "علی مرادی",
		miniName: "علی",
		role: "مسئول شیفت",
		miniRole: "م-ش",
		exit: "خروج",
	},
	expandible: {
		expand: "باز کردن منو",
		shrink: "جمع کردن منو",
	},

	footer: "تمامی حقوق متعلق به شرکت میلاد دارو نور است.",
};

export default contents;
