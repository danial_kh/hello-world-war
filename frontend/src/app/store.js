import { configureStore } from "@reduxjs/toolkit";
import counterReducer from "../features/counter/counterSlice";
import snackReducer from "../features/counter/snackSlice";
import menuReducer from "../features/menu/menuSlice";

export const store = configureStore({
	reducer: {
		counter: counterReducer,
		snack: snackReducer,
		menu: menuReducer,
	},
});
