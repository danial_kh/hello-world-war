import React from "react";
import { Grid } from "@material-ui/core";
import { makeStyles, useMediaQuery, useTheme } from "@material-ui/core";
import wave1 from "./wave1.svg";
import wave2 from "./wave2.svg";
import background from "./background.png";
import logotype from "./logotype.png";
import LoginForm from "../../templates/LoginForm/LoginForm";
import contents from "../../templates/LoginForm/contents";
const useStyles = makeStyles({
	root: {},
	content: {},
	rbox: {
		height: "90vh",
	},
	lbox: {
		backgroundImage: `url(${background})`,
		backgroundRepeat: "no-repeat",
		// backgroundSize: "cover",
		backgroundSize: "100%",

		borderRadius: "0 0 0 25px",
		overflow: "hidden",
		display: "flex",
		justifyContent: "center",
		alignItems: "flex-start",
	},
	logotype: {
		width: "40%",
		marginTop: "45%",
	},
	bbox: {
		height: "10vh",
		textAlign: "center",
	},
	wave1: {
		transform: "rotate(0deg) translateY(-20%) translateX(30%)",
		height: "80%",
		border: "1px solid gray",
		zIndex: "1",
	},
	wave2: {
		transform: "rotate(0deg) translateY(-20%) translateX(30%)",
		height: "80%",
		border: "1px solid gray",
	},
	footer: {
		fontWeight: 300,
		fontSize: "10pt",
		color: "#707070",
	},
});

export default function LoginDesktop() {
	const classes = useStyles();
	return (
		<Grid
			container
			justifyContent="space-between"
			alignItems="baseline"
			direction="row"
			className={classes.root}
		>
			<Grid container md={12} direction="row" className={classes.content}>
				<Grid item md={7} className={classes.rbox}>
					<LoginForm />
				</Grid>
				<Grid item md={5} className={classes.lbox}>
					<img src={logotype} className={classes.logotype} />
				</Grid>
			</Grid>
			<Grid item md={12} className={classes.bbox}>
				<h3 className={classes.footer}>{contents.footer}</h3>
			</Grid>
		</Grid>
	);
}
