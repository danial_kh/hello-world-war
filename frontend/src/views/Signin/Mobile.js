import React from "react";
import { Grid } from "@material-ui/core";
import LoginForm from "../../templates/LoginForm/LoginForm";
import { makeStyles } from "@material-ui/core";
import background from "./background.png";
import logotype from "./logotype.png";
import contents from "../../templates/LoginForm/contents";
const useStyles = makeStyles({
	root: {},
	content: {},
	rbox: {
		// height: "calc(90vh - ( 20vh))",
		// backgroundColor: "#fafafa",
	},
	topbox: {
		backgroundImage: `url(${background})`,
		backgroundRepeat: "no-repeat",
		// backgroundSize: "cover",
		backgroundSize: "100%",

		borderRadius: "0 0 0 50px",
		overflow: "hidden",
		display: "flex",
		justifyContent: "center",
		alignItems: "center",
		width: "100%",
		height: "20vh",
		minHeight: "70px",
	},
	logotype: {
		width: "40%",
	},
	bbox: {
		height: "56px",
		width: "100%",
		textAlign: "center",
		alignItems: "center",
		justifyContent: "center",
		position: "fixed",
		bottom: "0px",
		backgroundColor: "#fafafa",
		zIndex: "1",
	},
	wave1: {
		transform: "rotate(0deg) translateY(-20%) translateX(30%)",
		height: "80%",
		border: "1px solid gray",
		zIndex: "1",
	},
	wave2: {
		transform: "rotate(0deg) translateY(-20%) translateX(30%)",
		height: "80%",
		border: "1px solid gray",
	},
	footer: {
		fontWeight: 300,
		fontSize: "10pt",
		color: "#707070",
	},
});
export default function LoginMobile() {
	const classes = useStyles();
	return (
		<Grid container>
			<Grid item md={12} className={classes.topbox}>
				<img src={logotype} className={classes.logotype} />
			</Grid>
			<Grid component="div" item md={12} className={classes.rbox}>
				<LoginForm />
			</Grid>

			<Grid item md={12} className={classes.bbox}>
				<h3 className={classes.footer}>{contents.footer}</h3>
			</Grid>
		</Grid>
	);
}
