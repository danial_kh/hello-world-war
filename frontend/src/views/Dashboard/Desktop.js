import React from "react";
import { Grid, Card } from "@material-ui/core";
import { makeStyles, useMediaQuery, useTheme } from "@material-ui/core";
import DesktopFooter from "../../components/Footer/footer";
import DesktopMenu from "../../templates/DesktopMenu/DesktopMenu";
import MiniMenu from "../../templates/DesktopMenu/MiniMenu";
import { useSelector, useDispatch } from "react-redux";
import { selectMenu } from "../../features/menu/menuSlice";
import DashboardTemplate from "../../templates/Dashboard/DashboardTemplate";
const useStyles = makeStyles({
	root: {
		flexWrap: "nowrap",
		transition: "600ms all",
		minHeight: "100vh",
	},
	container: {
		minHeight: "100%",
		maxWidth: "100%",
	},
	menu: {
		maxWidth: "240px",
		minWidth: "240px",
		minHeight: "100%",
	},
	miniMenu: {
		maxWidth: "78px",
		minWidth: "78px",
		minHeight: "100%",
	},
	footer: {
		height: "36px",
		justifyContent: "center",
	},
	mainDashboard: {
		minHeight: "calc(100% - 93px)",
		margin: "36px 0 0 0",
	},
});

export default function DashboardDesktop() {
	const classes = useStyles();
	const menu = useSelector(selectMenu);
	function displayMiniMenu() {
		return <MiniMenu />;
	}
	function displayExpandedMenu() {
		return <DesktopMenu />;
	}
	return (
		<Grid container className={classes.root}>
			<Grid
				md={3}
				xl={2}
				className={menu.expand === "expand" ? classes.menu : classes.miniMenu}
			>
				{/* <MiniMenu /> */}
				{menu.expand === "expand" ? displayExpandedMenu() : displayMiniMenu()}
			</Grid>
			<Grid md={12} xl={12} className={classes.container} direction="column">
				<Grid md={12} className={classes.mainDashboard}>
					<DashboardTemplate />
				</Grid>
				<Grid md={12} className={classes.footer}>
					<DesktopFooter />
				</Grid>
			</Grid>
		</Grid>
	);
}
