from django.http import response
from django.shortcuts import render
from django.contrib.auth.models import User
from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.response import Response

import random
import datetime
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.authtoken.models import Token
from rest_framework.parsers import JSONParser, MultiPartParser

from .preverified import PRE_VERIFIED

# Create your views here.
class AuthenticateViewSet(ObtainAuthToken):
    authentication_classes = []
    permission_classes = []
    
    @action(detail=False, methods=["post"])
    def post(self, request, *args, **kwargs):
        
        user = User.objects.filter(username=request.data["username"])
        if user.exists():
            serializer = self.serializer_class(data=request.data,
                                            context={'request': request})
            serializer.is_valid(raise_exception=True)
            user = serializer.validated_data['user']
            token, created = Token.objects.get_or_create(user=user)
            return Response({
                'auth' : "2",
                'token': token.key,
            })
        else:
            
            #user2 =  FakhraUser.objects.filter(national_id=request.data["username"]).first()
            return Response({'auth': '1'})
        
#comment

